﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using CMS.Models.Models;

namespace CMS.Service.Utilities
{
    public class MembeshipContext
    {
        public IPrincipal Principal { get; set; }
        public Users User { get; set; }
        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
