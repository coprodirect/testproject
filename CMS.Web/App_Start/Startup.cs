﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin;
using Owin;
using Autofac;
using Autofac.Integration.Mvc;
using CMS.Data;
using CMS.Data.Infrastructure;
using CMS.Data.Repositories;
using CMS.Service;
using CMS.Service.Abstract;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;

[assembly: OwinStartup(typeof(CMS.Web.App_Start.Startup))]

namespace CMS.Web.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            ConfigAutofac(app);
        }

        private void ConfigAutofac(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
          
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            builder.RegisterType<CMSEntities>().AsSelf().InstancePerRequest();

            builder.RegisterType<EmailService>().As<IUserService>().SingleInstance();
            builder.RegisterType<EmailService>().As<IMembershipService>().SingleInstance();
            // Repositories
            builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();
            // Services
            builder.RegisterAssemblyTypes(typeof(UserService).Assembly)
               .Where(t => t.Name.EndsWith("Service"))
               .AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(MembershipService).Assembly)
             .Where(t => t.Name.EndsWith("Service"))
             .AsImplementedInterfaces().InstancePerRequest();
            Autofac.IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

           
        }
    }
}
