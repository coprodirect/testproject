﻿namespace CMS.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}