﻿namespace CMS.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private CMSEntities dbContext;

        public CMSEntities Init()
        {
            return dbContext ?? (dbContext = new CMSEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}