﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Models.Models;
using CMS.Data.Repositories;
using CMS.Data.Infrastructure;

namespace CMS.Data.UserExtension
{
    public static class UserExtension
    {
        public static Users GetSingleByUsername(this IRepository<Users> userRepository, string username)
        {
            return userRepository.GetAll().FirstOrDefault(x => x.UserName == username);
        }
    }
}
