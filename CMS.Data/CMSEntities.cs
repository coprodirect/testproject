﻿using CMS.Data.Configuration;
using CMS.Models.Models;
using System.Data.Entity;

namespace CMS.Data
{
    public class CMSEntities : DbContext
    {
        public CMSEntities() : base("CMSSeedData")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Pages> Pages { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        public static CMSEntities Create()
        {
            return new CMSEntities();
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new PageConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
        }
    }
}